
Pour faire l'appel du service REST sur Amazon :
http://sample-env-1.ikjqpbsrt2.us-east-1.elasticbeanstalk.com/position?lat=1&lng=2

Présentement le service retourne la valeur passé dans le paramètre "lat".

0=Usager n'est pas à un endroit spécifique.
1=Usager sur une piste cyclable.
2=Usager dans un parc.