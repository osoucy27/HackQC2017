package com.hack.ventnord.ws.interpol;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Test {

	public static void main(String[] args) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		PointRequest req = new PointRequest();
		req.setLat_1(1.0);
		req.setLon_1(2.0);
		req.setLat_2(3.0);
		req.setLon_2(4.0);
		
		String reqStr = mapper.writeValueAsString(req);
		
		
		PointResponse resp = new PointResponse();
		resp.setInterpolPoints(new ArrayList<Point>());
		Point p1 = new Point();
		p1.setLat(1.0);
		p1.setLon(2.0);
		Point p2 = new Point();
		p2.setLat(3.0);
		p2.setLon(4.0);
		resp.getInterpolPoints().add(p1);
		resp.getInterpolPoints().add(p2);
		
		String respStr = mapper.writeValueAsString(resp);
		
	}

}
