package com.hack.ventnord.ws.interpol;

import java.util.ArrayList;
import java.util.List;

import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PointInterpolation {
	
	@RequestMapping(value="/interpol", method = RequestMethod.POST)
	@CrossOrigin
	public PointResponse process(@RequestBody PointRequest input) throws NoSuchAuthorityCodeException, FactoryException{
		
		List<Point> l = new ArrayList<Point>();
		Point p1 = new Point(input.getLon_1(), input.getLat_1());
		Point p2 = new Point(input.getLon_2(), input.getLat_2());
		l.add(p1);
		l.add(p2);
		
		lineStringPointEveryMeters(l, input.getTime());
		l.remove(l.size()-1);
		
		PointResponse resp = new PointResponse();
		resp.setInterpolPoints(l);
		
		return resp;
		
	}
	
	private static void lineStringPointEveryMeters(List<Point> l, int split) throws NoSuchAuthorityCodeException, FactoryException{
		Point p1 = l.get(0);

		for(int i=1; i < l.size(); i++){
			Point p2 = l.get(i);

			double length = getLength(p1,p2);
			double ls = length / (split-1);

			while(length > ls){
				
				double ratio = ls / length;
				double dx = p2.getLon() - p1.getLon();
				double dy = p2.getLat() - p1.getLat();
				
				double newX = p1.getLon()+(dx*ratio);
				double newY = p1.getLat() +(dy*ratio);
				
				p1 = new Point(newX, newY);
				l.add(i, p1);
				i++;
				length = getLength(p1, p2);
				
			}
			
			
			p1 = p2;
		}
	}

	private static double getLength(Point p1, Point p2) throws NoSuchAuthorityCodeException, FactoryException {
		GeodeticCalculator calculator = new GeodeticCalculator(CRS.decode("EPSG:4326"));
		calculator.setStartingGeographicPoint(p1.getLon(), p1.getLat());
		calculator.setDestinationGeographicPoint(p2.getLon(), p2.getLat());
		
		double length = calculator.getOrthodromicDistance();
		
		return length;
	}

}
