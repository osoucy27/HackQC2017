package com.hack.ventnord.ws.interpol;

public class Point {
	
	double lon;
	double lat;
	
	public Point(){}
	
	public Point(double lon, double lat) {
		super();
		this.lon = lon;
		this.lat = lat;
	}
	
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}

}
