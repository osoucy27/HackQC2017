package com.hack.ventnord.ws.interpol;

import java.util.List;

public class PointResponse {
	
	List<Point> interpolPoints;
	

	public List<Point> getInterpolPoints() {
		return interpolPoints;
	}

	public void setInterpolPoints(List<Point> interpolPoints) {
		this.interpolPoints = interpolPoints;
	}

}
