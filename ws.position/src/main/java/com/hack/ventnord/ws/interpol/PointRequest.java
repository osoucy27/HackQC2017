package com.hack.ventnord.ws.interpol;

public class PointRequest {
	
	int time;
	double lon_1;
	double lat_1;
	double lon_2;
	double lat_2;
	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public double getLon_1() {
		return lon_1;
	}
	public void setLon_1(double lon_1) {
		this.lon_1 = lon_1;
	}
	public double getLat_1() {
		return lat_1;
	}
	public void setLat_1(double lat_1) {
		this.lat_1 = lat_1;
	}
	public double getLon_2() {
		return lon_2;
	}
	public void setLon_2(double lon_2) {
		this.lon_2 = lon_2;
	}
	public double getLat_2() {
		return lat_2;
	}
	public void setLat_2(double lat_2) {
		this.lat_2 = lat_2;
	}

}
