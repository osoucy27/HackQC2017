package com.hackqc.ventnord.ws.position;

import java.util.List;

public class UserPositionResponse {
	
	List<Integer> points;

	public List<Integer> getPoints() {
		return points;
	}

	public void setPoints(List<Integer> points) {
		this.points = points;
	}

}
