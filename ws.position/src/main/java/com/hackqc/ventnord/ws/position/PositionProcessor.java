package com.hackqc.ventnord.ws.position;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PositionProcessor {
	
	/**
	 * Le service retourne une liste de code.
	 * 
	 * Si l'usager est sur une piste cyclable la liste contient "1".
	 * 
	 * @param lat
	 * @param lng
	 * @return
	 */
	@RequestMapping("/position")
	@CrossOrigin
	public UserPositionResponse process(@RequestParam(value="lat") String lat, @RequestParam(value="lng") String lng){
		
		UserPositionResponse resp = new UserPositionResponse();
		resp.setPoints(new ArrayList<Integer>());
		
		if(isPisteCyclable(lng, lat)){
			resp.getPoints().add(1);
		}
		
		if(isParc(lng,lat)){
			resp.getPoints().add(2);
		}
		
		if(isPieton(lng,lat)){
			resp.getPoints().add(3);
		}
		
		if(isEdifice(lng,lat)){
			resp.getPoints().add(4);
		}
		
		return resp;
	}
	
	private boolean isEdifice(String lng, String lat) {
		Connection connection=null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
			
			String sql = "select object_id "
							+"from polygon z "
							+"where ST_Intersects(z.polygon, ST_GeographyFromText(?))"
							+ "and file = ?;";
			
			PreparedStatement st = connection.prepareStatement(sql);
			st.setString(1, "POINT("+lng + " " + lat+")");
			st.setString(2, "edificesmunicipaux.json");
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt(1);
				return true;
			}
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		
		return false;
	}

	private boolean isParc(String lng, String lat) {
		
		Connection connection=null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
			
			String sql = "select object_id "
							+"from polygon z "
							+"where ST_Intersects(z.polygon, ST_GeographyFromText(?))"
							+ "and file = ?;";
			
			PreparedStatement st = connection.prepareStatement(sql);
			st.setString(1, "POINT("+lng + " " + lat+")");
			st.setString(2, "parcespacevert.json");
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt(1);
				return true;
			}
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		
		return false;	
	}

	private boolean isPisteCyclable(String lng, String lat){
		
		Connection connection=null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
			
			String sql = "select object_id "
							+"from linestring z "
							+"where ST_DWITHIN(Geography(ST_Transform(z.line,4326)), ST_GeographyFromText(?),2)"
							+ " and file = ?;";
			
			PreparedStatement st = connection.prepareStatement(sql);
			st.setString(1, "POINT("+lng + " " + lat+")");
			st.setString(2, "pistecyclable.json");
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt(1);
				return true;
			}
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		
		return false;	
	}
	
	private boolean isPieton(String lng, String lat){
		Connection connection=null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
			
			String sql = "select object_id "
							+"from linestring z "
							+"where ST_DWITHIN(Geography(ST_Transform(z.line,4326)), ST_GeographyFromText(?),2)"
							+ " and file = ?;";
			
			PreparedStatement st = connection.prepareStatement(sql);
			st.setString(1, "POINT("+lng + " " + lat+")");
			st.setString(2, "infrapieton.json");
			
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt(1);
				return true;
			}
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		
		return false;	
	}
}
