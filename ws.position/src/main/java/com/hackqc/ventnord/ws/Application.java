package com.hackqc.ventnord.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.hack.ventnord.ws.interpol.PointInterpolation;
import com.hackqc.ventnord.ws.position.PositionProcessor;

@SpringBootApplication
@ComponentScan(basePackageClasses = PointInterpolation.class)
@ComponentScan(basePackageClasses = PositionProcessor.class)
public class Application {
	
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
