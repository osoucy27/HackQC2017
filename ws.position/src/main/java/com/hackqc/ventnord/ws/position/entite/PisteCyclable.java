package com.hackqc.ventnord.ws.position.entite;

public class PisteCyclable {
	public String getType() {
		return type;
	}

	final int objectId;
	final double length;
	final String typeRevtCyc;
	final String typeVoieCyc;
	final String lng;
	final String lat;
	final String type;
	
	public PisteCyclable(int objectId, double length, String typeRevtCyc, String typeVoieCyc, String lng, String lat, String type) {
		super();
		this.objectId = objectId;
		this.length = length;
		this.typeRevtCyc = typeRevtCyc;
		this.typeVoieCyc = typeVoieCyc;
		this.lng = lng;
		this.lat = lat;
		this.type = type;
	}

	public int getObjectId() {
		return objectId;
	}

	public double getLength() {
		return length;
	}

	public String getTypeRevtCyc() {
		return typeRevtCyc;
	}

	public String getTypeVoieCyc() {
		return typeVoieCyc;
	}

	public String getLng() {
		return lng;
	}

	public String getLat() {
		return lat;
	}


}
