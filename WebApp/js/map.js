var Rimouski = {
    longitude:-68.5232,
    latitude:48.4525
}
    
    /**
     * View Controller
     * @type {Object}
     */
    var controller = controller || {};

    /////
    // PUBLIC Variables
    /////

    controller.map = null;
    controller.helper = null;
    controller.deviceReady = false;
    controller.markerSymbol = null;
    controller.utils = {};

    /**
     * A property indicating a reference to the current view.
     * Default = null
     * @type {String}
     */
    controller.viewChange = null;
    /**
     * Allows points to accumulate on map. Default is true.
     * @type {boolean}
     */
    controller.accumulate = true;
    /**
     * Geolocation timeout property for watchPosition
     * Default = 60000
     * @type {number}
     */
    controller.timeout = 60000;
    /**
     * Geolocation maximumAge property for watchPosition
     * Default = 60000
     * @type {number}
     */
    controller.maximumAge = 60000;
    /**
     * Geolocation timeout property for currentPosition
     * Default = 60000
     * @type {number}
     */
    controller.timeoutCurrentPos = 60000;
    /**
     * Geolocation maximumAge property for currentPosition.
     * Default = 60000
     * @type {number}
     */
    controller.maxAgeCurrentPos = 60000;
    /**
     * Sets the map zoom level. Default = 14;
     * @type {int}
     */
    controller.zoomLevel = 17;
    /**
     * If set to false then alerts will be silent and errors written to the console.
     * @type {boolean}
     */
    controller.useAlerts = true;

    /**
     * Local Storage ENUMs
     * @type {Object}
     */
    controller.localStorageEnum = (function(){
        var values = {
            ZOOM_LEVEL:"zoom_level",
            LAT:"lat",
            LON:"lon",
            MAP_WIDTH:"map_width",
            MAP_HEIGHT:"map_height",
            PORTRAIT:"portrait",
            LANDSCAPE:"landscape",
            GEOLOCATION:true,
            AFFICHAGE_ESPACESVERTS:true,
            AFFICHAGE_RESEAUPIETONNIER:true,
            AFFICHAGE_PISTESCYCLABLES:true,
            AFFICHAGE_ATTRAITSTOURISTIQUES:true
        }

        return values;
    });
    
    controller.geoJSONDataset = {
        "parcespacevert": {
            url : "https://s3.amazonaws.com/hackqcriki/data/parcespacevert.json",
            color: [57, 165, 29, 0.5],
            symbol: null},            
        "pistecyclable":  {
            url : "https://s3.amazonaws.com/hackqcriki/data/pistecyclable.json",
            color: [51, 122, 183, 0.9],
            symbol: null},
        "InfraPieton": {
            url : "https://s3.amazonaws.com/hackqcriki/data/infrapieton.json",
            color: [169, 68, 66, 0.9],
            symbol: null},
        "edificesmunicipaux": {
            url : "https://s3.amazonaws.com/hackqcriki/data/edificesmunicipaux.json",
            color: [224, 179, 0, 0.9],
            symbol: null}
    };
    
    

    controller.init = (function() {

        /**
         * Does browser support localStorage?
         * Always validate HTML5 functionality :-)
         */
        (function (){
            var support = null;
            try {
                support = 'localStorage' in window && window['localStorage'] !== null;
            } catch (e) {
                support = false;
            }
            controller._supportsLocalStorage = support;
        }).bind(this)();

        controller.map = new esri.Map("map",{
            basemap: "topo",
            center: [Rimouski.longitude, Rimouski.latitude],
            slider: true,
            zoom: 2
        });

        //Listen for map zoom end events
        controller._on(controller.map, "zoom-end", function (object){
            controller.zoomLevel = object.level;
        })

        //Listen for when the map loads
        controller._on(controller.map,"load", function () {

            //Adjust the map's zoom slider
            var zoom_slider_left = parseFloat($('#map_zoom_slider').css('left'));
            var zoom_slider_width = parseFloat($('#map_zoom_slider').css('width'));
            $('#div1').css('left',zoom_slider_left * 2 + zoom_slider_width + "px");

            // Initiate jQueryHelper library
            // Auto-recenters on device rotation
            controller.helper = new jQueryHelper(controller.map);

            //Some browsers don't show full height after onLoad
            controller.map.reposition();
            controller.map.resize();

            
            
            if(controller.deviceReady == true){
                var isGeolocationActive = localStorage.getItem(controller.localStorageEnum().GEOLOCATION);
                if ( isGeolocationActive != undefined && isGeolocationActive == "true") {
                    controller.startGeolocation();
                }else{
                    // If Geolocation is not active, we listen on pubnub channel for position update from bots
                    controller.pnChannel = "HackQc-user";
                    controller.pubnub = new PubNub({
                      publishKey: 'pub-c-5edebcc3-1df8-43f6-a6d7-61eb18c45821',
                      subscribeKey: 'sub-c-aa82392a-0608-11e7-b34d-02ee2ddab7fe'
                    });    
                    controller.pubnub.subscribe({channels: [controller.pnChannel]});
                    controller.pubnub.addListener({message:controller.processBotsLocationResult});
                    
                    // Focus the view on Rimouski by default
                    controller.updateLocationOnMap(Rimouski.longitude, Rimouski.latitude);
                }
            }
            else{
                console.log("LocationHelper.js: device not ready - unable to start geolocation.");
                console.log("Device not ready - unable to start geolocation.");
            }

            //Handle orientation events to allow for resizing the map and working around
            //jQuery mobile bugs related to how and when the view settles after such an event
            var supportsOrientationChange = "onorientationchange" in window,
                    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

            window.addEventListener(orientationEvent, function () {
                controller.rotateScreen(400,true);
            }, false);

            //Fix bug in jQuery that destroys map when child view is rotated
            $("div:jqmData(role='page')").on('pagehide',function(){
                controller.viewChange = this.id;
                console.log('view changed ' + controller.map.height + ", " + controller.map.width);
                controller.rotateScreen(400,true);
            });

        });

        controller.map.addLayer(controller._locatorMarkerGraphicsLayer);
        
        // Get the GeoJSON to add - file or endpoint will work
        function getGeoJSON(url) {
          var requestHandle;
            // xhr request to get data 
            requestHandle = controller._esriRequest({
                url: url,
                handleAs: "json"
            });
            requestHandle.then(addGeoJSONLayer, errorLoadingGeoJSON);
        }

        // GeoJSON to ArcGIS geometry type
        function getEsriGeometryType(geometryGeoJson) {
          var type;
          switch (geometryGeoJson) {
            case "Point":
              type = "esriGeometryPoint";
              break;
            case "MultiPoint":
              type = "esriGeometryMultipoint";
              break;
            case "LineString":
              type = "esriGeometryPolyline";
              break;
            case "Polygon":
              type = "esriGeometryPolygon";
              break;
            case "MultiPolygon":
              type = "esriGeometryPolygon";
              break;
          }
          return type;
        }

        function getEsriSymbol(geometryType, datasetName) {
          var sym;
          if (datasetName != undefined && controller.geoJSONDataset[datasetName] != undefined){
              return controller.geoJSONDataset[datasetName].symbol;
          }
          // Not used for now
          /*switch (geometryType) {
            case "esriGeometryPoint":
              sym = simplePointSym;
              break;
            case "esriGeometryMultipoint":
              sym = simplePointSym;
              break;
            case "esriGeometryPolyline":
              sym = simpleLineSym; 
              break;
            case "esriGeometryPolygon":
              sym = simplePolygonSym;
              break;
            case "esriGeometryPolygon":
              sym = simplePolygonSym;
              break;
          }*/
          return sym;
        }

        function createFeatureCollection(geometryType) {
          // TODO - need to pull definition from GeoJSON attributes!
          var layerDefinition = {
            "geometryType": geometryType,
            "objectIdField": "ObjectID",
            "fields": [{
                "name": "ObjectID",
                "alias": "ObjectID",
                "type": "esriFieldTypeOID"
              }, {
                "name": "type",
                "alias": "Type",
                "type": "esriFieldTypeString"
              }]
          }; 
          var featureCollection = {
            layerDefinition: layerDefinition,
            featureSet: {
              features: [],
              geometryType: geometryType
            }
          };
          return featureCollection;
        }

        function createFeature(arcgis, sym) {
          var shape = new esri.Graphic(arcgis).setSymbol(sym);
          // Do other things with shape...  
          return shape;
        }

        // Use Terraformer to convert geojson to arcgis json
        function createFeatureSet(geojson, geometryType){
          var featureSet = [];
          // convert the geojson object to a arcgis json representation
          var primitive = new Terraformer.FeatureCollection(geojson);
          var arcgis = Terraformer.ArcGIS.convert(primitive);
          // NOTE: Assumes same geojson geometry type for entire file!
          var sym = getEsriSymbol(geometryType, geojson.name);
          for (var i = 0; i < arcgis.length; i++){
            featureSet.push(createFeature(arcgis[i], sym));
          }
          return featureSet;
        }


        // Create a feature layer for the GeoJSON
        function addGeoJSONLayer(geojson) {
            if (!geojson.features.length) {
                return;
            }
            // Get geometry type - assume same geometry for entire file!
            var esriGeometryType = getEsriGeometryType(geojson.features[0].geometry.type);
            // Create an skeleton collection and popup definition
            var featureCollection = createFeatureCollection(esriGeometryType);
            var infoTemplate = new controller._infoTemplate("GeoJSON Data", "${*}");
            // Create feature layer   
            controller.featureLayer[geojson.name] = new controller._featureLayer(featureCollection, {
                mode: controller._featureLayer.MODE_SNAPSHOT,
                outFields: ["*"], 
                infoTemplate: infoTemplate
            });
            // Add it to the map
            controller.map.addLayer(controller.featureLayer[geojson.name]);
            // Add graphics to the featurelayer
            var featureSet = createFeatureSet(geojson, esriGeometryType);
            controller.featureLayer[geojson.name].applyEdits(featureSet, null, null);
        }

        // Remove existing layer        
        controller.removeGeoJSONLayer = function (layerName) {
            if (controller.featureLayer[layerName]) {
                controller.map.removeLayer(controller.featureLayer[layerName]);
            }
        }

        // Error
        function errorLoadingGeoJSON(e) {
            console.log("Error loading GeoJSON. " + e);
        }

        // Load the GeoJson layers
        getGeoJSON(controller.geoJSONDataset["parcespacevert"].url);
        getGeoJSON(controller.geoJSONDataset["pistecyclable"].url); 
        getGeoJSON(controller.geoJSONDataset["InfraPieton"].url);    
        getGeoJSON(controller.geoJSONDataset["edificesmunicipaux"].url);    
    });
   
        
    /**
     * Not currently used
     * @type {Function}
     */
    controller.timeoutCurrentPoschange = (function(){
        controller.timeoutCurrentPos = $("#timeout-current-pos").val();
        console.log(controller.timeoutCurrentPos);
    });

    controller.restartGeo = (function restartGeo(){
        var test = confirm("Restart Geolocation?");
        if(test){
            var maxAgeCurrentPos = $("#max-age-current-pos").val();
            var timeoutCurrentPos = $("#timeout-current-pos").val();
            var maxAge = $("#max-age-watch-pos").val();
            var timeout = $("#timeout-watch-pos").val();
            if(controller.isNumber(maxAgeCurrentPos) && controller.isNumber(timeoutCurrentPos) &&
                controller.isNumber(maxAge) && controller.isNumber(timeout)){
                    controller.maxAgeCurrentPos = maxAgeCurrentPos;
                    controller.timeoutCurrentPos = timeoutCurrentPos;
                    controller.timeout = timeout;
                    controller.maximumAge = maxAge;
                    controller.startGeolocation();
            }
            else{
                alert("Check all your inputs they must be numeric.")
            }
        }
    });

    controller.toggleAccumulate = (function(evt){
        if(evt.target.id == "slider-accumulate-pts" && evt.target.value == "on"){
            controller.accumulate = true;
        }
        else{
            controller.accumulate = false;
        }
    });

    controller.isNumber = (function(value){
        return !isNaN(parseFloat(value)) && isFinite(value);
    });

    controller.showLocation = (function(){
        if($('#div1').is(':visible') == false || $('#div1').is(':hidden')){
            $('#div1').show();
        }
        else{
            $('#div1').hide();
        }

    });

    /**
     * Public function for shutting down the geolocation service.
     */
    controller.shutOffGeolocation = (function(evt){

        if(evt.target.id == "slider-geo-on-off" && evt.target.value == "on"){
            localStorage.setItem(controller.localStorageEnum().GEOLOCATION, true);     
            if (controller.pubnub != undefined){      
                controller.pubnub.unsubscribe({
                    channels: [controller.pnChannel]
                });
            }
            controller.restartGeo();
        }
        else{
            localStorage.setItem(controller.localStorageEnum().GEOLOCATION, false);
            controller.stopGeolocation();
            alert("Geolocation has been shutoff");
        }
    });
    
    controller.togglePisteCyclable = (function(evt){
        if(evt.target.id == "slider-pistes-cyclables" && evt.target.value == "off"){
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_PISTESCYCLABLES, true); 
            controller.removeGeoJSONLayer("pistecyclable");
        }
        else{
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_PISTESCYCLABLES, false);
            controller.map.addLayer(controller.featureLayer["pistecyclable"]);
        }
    });
    controller.toggleReseauPietonier = (function(evt){
        if(evt.target.id == "slider-reseau-pietonnier" && evt.target.value == "off"){
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_RESEAUPIETONNIER, true); 
            controller.removeGeoJSONLayer("InfraPieton");
        }
        else{
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_RESEAUPIETONNIER, false);
            controller.map.addLayer(controller.featureLayer["InfraPieton"]);
        }
    });
    controller.toggleEspaceVerts = (function(evt){
        if(evt.target.id == "slider-espaces-verts" && evt.target.value == "off"){
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_ESPACESVERTS, true); 
            controller.removeGeoJSONLayer("parcespacevert");
        }
        else{
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_ESPACESVERTS, false);
            controller.map.addLayer(controller.featureLayer["parcespacevert"]);
        }
    });
    controller.toggleAttraitsTouritiques = (function(evt){
        if(evt.target.id == "slider-attraits-touristiques" && evt.target.value == "off"){
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_ATTRAITSTOURISTIQUES, true); 
            controller.removeGeoJSONLayer("edificesmunicipaux");
        }
        else{
            localStorage.setItem(controller.localStorageEnum().AFFICHAGE_ATTRAITSTOURISTIQUES, false);
            controller.map.addLayer(controller.featureLayer["edificesmunicipaux"]);
        }
    });

    /**
     * Public function for toggling the geolocation high-accuracy property on/off
     */
    controller.toggleHighAccuracy = (function(evt){

        if(evt.target.id == "slider-high-accuracy" && evt.target.value == "on"){
            controller.setHighAccuracy(true);
        }
        else{
            controller.setHighAccuracy(false);
        }

    });

    controller.toggleErrorAlerts = (function(evt){
        if(evt.target.id == "slider-handle-errs" && evt.target.value == "on"){
            controller.useAlerts = true;
        }
        else{
            controller.useAlerts = false;
        }
    });

    /**
     * Turns on the geolocation capabilities.
     */
    controller.startGeolocation = function(){

        var _dateStart = new Date();
        var _previousDate = null;

        if(controller._supportsLocalStorage){
            try{
                localStorage.setItem(controller.localStorageEnum().MAP_WIDTH,$("#map").width());
                localStorage.setItem(controller.localStorageEnum().MAP_HEIGHT,$("#map").height());
                window.innerHeight > window.innerWidth ?
                        controller._orientation = controller.localStorageEnum().PORTRAIT :
                        controller._orientation = controller.localStorageEnum().LANDSCAPE;
            }
            catch(err){
                console.log("_supportsLocationStorage: " + err.message);
            }
        }

        try{
            if (navigator.geolocation) {
                console.log("setHighAccuracy = " + controller._setHighAccuracy);

                navigator.geolocation.getCurrentPosition(
                        _processGeolocationResult.bind(this)/* use bind() to maintain scope */,
                        _html5Error.bind(this),
                        {
                            maximumAge: controller.maxAgeCurrentPos,
                            timeout: controller.timeoutCurrentPos,
                            enableHighAccuracy: controller._setHighAccuracy
                        }
                );

                controller._watchID = navigator.geolocation.watchPosition(
                        _processGeolocationResult.bind(this),
                        _html5Error.bind(this),
                        {
                            timeout: controller.timeout,
                            enableHighAccuracy: controller._setHighAccuracy,
                            maximumAge: controller.maximumAge
                        }
                );

                //For more info on maximumAge (milliseconds): http://dev.w3.org/geo/api/spec-source.html#max-age
                //For more info on timeout (milliseconds): http://dev.w3.org/geo/api/spec-source.html#timeout
                //For more info on enableHighAccuracy: http://dev.w3.org/geo/api/spec-source.html#high-accuracy
            }
            else {
                alert("Sorry, your browser does not support geolocation");
            }
        }
        catch(err){
            alert("Unable to start geolocation. See console.log for message.")
            console.log("startGeolocation: " + err.message);
        }

        /**
         * Post-process the results from a geolocation position object.
         * @param position
         * @private
         */
        function _processGeolocationResult(position) {

            var html5Lat = position.coords.latitude; //Get latitude
            var html5Lon = position.coords.longitude; //Get longitude
            var html5TimeStamp = position.timestamp; //Get timestamp
            var html5Accuracy = position.coords.accuracy; 	//Get accuracy in meters
            var html5Heading = position.coords.heading;
            var html5Speed = position.coords.speed;
            var html5Altitude = position.coords.altitude;

            console.log("success " + html5Lat + ", " + html5Lon);
            $("#geo-indicator").text("Geo: ON");
            $("#geo-indicator").css('color','green');

            //Set jQueryHelper properties
            controller.helper.setCenterPt(position.coords.latitude,position.coords.longitude,4326);
            controller.helper.setZoom(9);

            controller._displayGeocodedLocation(position);

            if (position.coords.latitude != null && position.coords.longitude != null) {
                //zoomToLocation(html5Lat, html5Lon);

                //Don't allow string to get beyond certain length.
                //Otherwise you'll create a huge memory leak. :-)
                if(controller._accuracyDataCSV.length < 50000){

                    var newDateDiff = null;
                    var ms = null;
                    var dateNow = new Date();
                    var totalElapsedTime =  _getTimeDifference(new Date(Math.abs(dateNow.getTime() - _dateStart.getTime())));

                    _previousDate == null ?
                            newDateDiff = new Date(Math.abs(dateNow.getTime() - _dateStart.getTime())) :
                            newDateDiff = new Date(Math.abs(dateNow.getTime() - _previousDate.getTime()));

                    _previousDate = new Date();

                    var dateResultString = _getTimeDifference(newDateDiff);

                    controller._accuracyDataCSV = controller._accuracyDataCSV + Date(html5TimeStamp).toLocaleString() +
                            "," + html5Lat +
                            "," + html5Lon +
                            "," + html5Accuracy +
                            "," + controller._setHighAccuracy +
                            "," + html5Altitude +
                            "," + html5Heading +
                            "," + html5Speed +
                            "," + position.coords.altitudeAccuracy +
                            "," + dateResultString +
                            "," + totalElapsedTime +
                            ",\r\n";
                }
                else{
                    console.log("Truncating CSV string. It is too long");
                }

                if(html5Lat != 0){
                    controller.updateLocationOnMap(html5Lon, html5Lat);
                }
            }
        }
                
        // Use this if you want to publish location to pubnub for external appllication
        function _publishGeolocationResult(position) {
           controller.pubnub.publish({channel:controller.pnChannel, message:{latitude:position.coords.latitude, 
                                                       longitude:position.coords.longitude,
                                                       accuracy:position.coords.accuracy,
                                                       loc_timestamp:position.timestamp,
                                                       heading:position.coords.heading,
                                                       speed:position.coords.speed,
                                                       altitude:position.coords.altitude}});
    
        }  
        
        /**
         * Calculate HH:MM:SS:MS from a given Date in millis
         * @param date
         * @returns {string}
         * @private
         */
        function _getTimeDifference(/* Date */ date){;
            var msec = date;
            var hh = Math.floor(msec / 1000 / 60 / 60);
            msec -= hh * 1000 * 60 * 60;
            var mm = Math.floor(msec / 1000 / 60);
            msec -= mm * 1000 * 60;
            var ss = Math.floor(msec / 1000);
            msec -= ss * 1000;

            hh = hh < 10 ? "0" + hh : hh;
            mm = mm < 10 ? "0" + mm : mm;
            ss = ss < 10 ? "0" + ss : ss;
            msec = msec < 10 ? "0" + msec : msec;

            console.log("time: " + hh + ":" + mm + ":" + ss + ":" + msec);

            return hh + ":" + mm + ":" + ss + ":" + msec;
        }

        /**
         * Handle geolocation service errors
         * @param error
         * @private
         */
        function _html5Error(error) {
            var error_value = "null";

            switch(error.code){
                case 1:
                    error_value = "PERMISSION_DENIED";
                    $("#slider-geo-on-off").val("off");
                    break;
                case 2:
                    error_value = "POSITION_UNAVAILABLE";
                    break;
                case 3:
                    //Read more at http://dev.w3.org/geo/api/spec-source.html#timeout
                    error_value = "TIMEOUT";
                    break;
            }

            controller.useAlerts == true ?
                    alert('There was a problem retrieving your location: ' + error_value) :
                    console.log('There was a problem retrieving your location: ' + error_value);
        }

    }
    
    controller.processBotsLocationResult = function(payload) {
      var latitude = payload.message.latitude;
      var longitude = payload.message.longitude;
      
      controller.updateLocationOnMap(longitude, latitude);   
    }
    
    controller.updateLocationOnMap = function(longitude, latitude) {
        //var wgsPt = new esri.geometry.Point(longitude, latitude);
        controller._mostRecentLocation =  new esri.geometry.Point(longitude, latitude);
        //this._map.centerAndZoom(this._mostRecentLocation, 14);
        controller._showLocation(longitude, latitude,controller._mostRecentLocation);

        if(controller._supportsLocalStorage){
            localStorage.setItem(controller.localStorageEnum().LAT,latitude);
            localStorage.setItem(controller.localStorageEnum().LON,longitude);
            localStorage.setItem(controller.localStorageEnum().ZOOM_LEVEL,controller.map.getZoom());
        }
                                                                     //Date.now() return timestamp in ms.
        player.accumulateWithNewPos({lat: latitude, lon: longitude}, Date.now()/1000);

        console.log('false, ' +
                localStorage.getItem(controller.localStorageEnum().MAP_WIDTH) + ", " +
                localStorage.getItem(controller.localStorageEnum().MAP_HEIGHT) + ", " +
                localStorage.getItem(controller.localStorageEnum().ZOOM_LEVEL) + ", " +
                this.map.getZoom()
        ); 
    }

    /**
     * Write geolocation values to UX
     * @param position
     * @private
     */
    controller._displayGeocodedLocation = function(position) {
        var altitude = "n/a";
        if(position.coords.altitude != null) altitude = position.coords.altitude.toFixed(2) + "m";
        var speed = "n/a";
        if (position.coords.speed != null) (position.coords.speed * 3600 / 1000).toFixed(2) + "km/hr";
        var heading = "n/a";
        if (position.coords.heading != null) position.coords.heading.toFixed(2) + "deg";

        $("#location").text(position.coords.latitude.toFixed(4) + ", " + position.coords.longitude.toFixed(4));
        $("#altitude").text("Alt: " + altitude);
        $("#speed").text("Spd: " + speed);
        $("#heading").text("Hdg: " + heading);

        //Tested on desktop IE9, Chrome 17, Firefox 10, Safari ?
        //Mobile browser: Android 2.3.6
        //There is a bug in Safari browsers on Mac that shows the year as 1981
        //To get around the bug you could manually parse and then format the date. I chose not to for this demo.
        var date = new Date(position.timestamp)
        $("#timestamp").text(date.toLocaleString());
        $("#accuracy").text("Acc: " + position.coords.accuracy.toFixed(2) + "m");

    }

    controller._showLocation = function(/* number */myLat,/* number */myLong,/* esri.geometry.Geometry */geometry) {

        var myPositionSymbol = null;
        var locatorSymbol = null;

        if(window.devicePixelRatio >= 2){
//            myPositionSymbol = controller._pushPinLarge;
            locatorSymbol = controller._locatorMarkerLarge;
        }
        else{
//            myPositionSymbol = controller._pushPinSmall;
            locatorSymbol = controller._locatorMarkerSmall;
        }

        //    map.infoWindow.setTitle("HTML5 Location");
        //    map.infoWindow.setContent('Lat : ' + myLat.toFixed(4) + ", " + ' Long: ' + myLong.toFixed(4));
        //    map.infoWindow.resize(200,65);
        //    map.infoWindow.show(mapPoint, esri.dijit.InfoWindow.ANCHOR_LOWERLEFT);

        controller.map.graphics.clear();
        controller.map.graphics.add(new esri.Graphic(geometry, controller.markerSymbol));
        controller.map.centerAndZoom(geometry, controller.zoomLevel);

        if(controller.accumulate == true)controller._locatorMarkerGraphicsLayer.add(new esri.Graphic(geometry, locatorSymbol));

    };

    controller.setHighAccuracy = function(value){
        controller.stopGeolocation();
        controller._setHighAccuracy = value;
        controller.restartGeo();
    }

    /**
     * Executes a clearWatch() to cease all watchPosition() activity.
     */
    controller.stopGeolocation = (function(){
        try{
            navigator.geolocation.clearWatch(controller._watchID);
            controller._watchID = null;
            $("#geo-indicator").text("Geo: OFF");
            $("#geo-indicator").css('color','red');
            
        }
        catch(err){
            console.log("stopGeolocation error: " + err.toString());
        }
    });

    /**
     * RECOMMENDATION FOR PHONEGAP - set property in manifest to prevent screen rotation!
     * Repositions the map after a screen rotation. Note there are bugs in jQuery
     * related to screen rotation when you are in a child view.
     * These bugs can affect the map and its associated properties.
     * @param value Timer delay property in ms
     * @param useLocalStore boolean sets whether or not to use localStorage values to recenter map.
     */
    controller.rotateScreen = (function(value,/* boolean */ useLocalStore){
        if(typeof useLocalStore == 'undefined') useLocalStore = false;

        console.log('rotateScreen, ' +
                localStorage.getItem(controller.localStorageEnum().MAP_WIDTH) + ", " +
                localStorage.getItem(controller.localStorageEnum().MAP_HEIGHT) + ", " +
                localStorage.getItem(controller.localStorageEnum().ZOOM_LEVEL));

        try{
            if(controller.viewChange == null || controller.viewChange == "settings"){

                var timeout = null;
                value != "undefined" ? timeout = value : timeout = 500;
                setTimeout((function(){
                    if(controller.map != null && controller._mostRecentLocation != null){

                        //NOTE: This attempts to fix a bug in jQuery where rotating
                        //device from a child window resets map div values and other map properties.
                        //TO-DO Still needs some tweaking -- not perfect.
                        if(controller.map.height == 0 || controller.map.width == 0){
                            if(useLocalStore == false){
                                if(controller._orientation == controller.localStorageEnum().PORTRAIT)
                                {
                                    controller.map.width = localStorage.getItem(controller.localStorageEnum().MAP_WIDTH);
                                    controller.map.height = localStorage.getItem(controller.localStorageEnum().MAP_HEIGHT);
                                }
                                else{
                                    controller.map.width = localStorage.getItem(controller.localStorageEnum().MAP_HEIGHT);
                                    controller.map.height = localStorage.getItem(controller.localStorageEnum().MAP_WIDTH);
                                }

                                controller.map.resize();
                                controller.map.reposition();
                                //map.width = screen.width;

                                var wgsPt = new esri.geometry.Point(
                                        localStorage.getItem(controller.localStorageEnum().LON),
                                        localStorage.getItem(controller.localStorageEnum().LAT), new esri.SpatialReference({ wkid: 4326 }))

                                controller.map.centerAndZoom(
                                        esri.geometry.geographicToWebMercator(wgsPt),
                                        localStorage.getItem(controller.localStorageEnum().ZOOM_LEVEL)
                                );
                            }
                        }
                        else{
                            controller.map.resize();
                            controller.map.reposition();

                            if(useLocalStore == false){
                                var wgsPt = new esri.geometry.Point(
                                        localStorage.getItem(controller.localStorageEnum().LON),
                                        localStorage.getItem(controller.localStorageEnum().LAT)
                                );
                                //map.width = screen.width;
                                controller.map.centerAndZoom(esri.geometry.geographicToWebMercator(wgsPt), controller.zoomLevel);
                            }
                        }
                    }

                }).bind(this),timeout);
            }
        }
        catch(err){
            console.log("rotateScreen() error " + err.message);
        }

    });

    /**
     * Gets a CSV string compiled from the geolocation service that includes
     * date,lat,lon,accuracy,high_accuracy_boolean,altitude,heading,speed,
     * altitude_accuracy,interval_time,total_elapsed_time
     * @returns {string}
     */
    controller.utils.getAccuracyCSV = (function(){
        return controller._accuracyDataCSV;
    });

    /**
     * Public function for emailing a CSV string containing geolocation service information.
     */
   controller.utils.sendEmail = (function(){
        window.open('mailto:'+ controller._MAIL_TO_ADDRESS +'?subject=HTML5 Accuracy Data&body=' +
                encodeURIComponent(controller.utils.getAccuracyCSV()));
    });

    require(["esri/config",
      "esri/map",
      "esri/graphic",
      "esri/layers/FeatureLayer",
      "esri/InfoTemplate",
      "esri/graphicsUtils",
      "esri/Color",
      "esri/symbols/SimpleMarkerSymbol",
      "esri/symbols/SimpleLineSymbol",
      "esri/symbols/SimpleFillSymbol",
      "esri/request",
      "esri/geometry/Point",
      "esri/geometry/Multipoint",
      "esri/geometry/Polyline",
      "esri/geometry/Polygon",
      "esri/symbols/PictureMarkerSymbol",
      "dojo/keys",
      "dojo/on",
      "dojo/dom",
      "dojo/domReady!"
      ],
            function (esriConfig, Map, Graphic, FeatureLayer, InfoTemplate, graphicsUtils, Color, SimplePointSymbol, SimpleLineSymbol, SimpleFillSymbol, esriRequest, Point, MulitPoint, Polyline, Polygon, PictureMarkerSymbol, keys, on, dom) {
            "use strict";

                /////
                // PRIVATE Variables
                /////

                controller._esriRequest = esriRequest;
                controller._infoTemplate = InfoTemplate
                controller._featureLayer = FeatureLayer
                controller._on = on;   //event listeners are set by 'on'
                controller._MAIL_TO_ADDRESS = "stephane@brillant.ca";
                controller._supportsLocalStorage = false;
                controller._watchID = null;
                controller._setHighAccuracy = true;
                controller._mostRecentLocation = null;
                controller._accuracyDataCSV = "Date,Lat,Long,Accuracy,High_accuracy_boolean,Altitude,Heading,Speed,Altitude_accuracy,Interval_time,Total_elapsed_time,\r\n";
                controller._pushPinLarge = new esri.symbol.PictureMarkerSymbol("images/pushpin104x108.png", 104, 108);
                controller._pushPinSmall = new esri.symbol.PictureMarkerSymbol("images/pushpin52x54.png", 48, 48);
                controller._locatorMarkerLarge = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE,
                        10,
                        new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 1),
                        new Color([255,255,0,0.5]));

                controller._locatorMarkerSmall = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE,
                        5,
                        new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new Color([0,0,0]), 0.5),
                        new Color([255,255,0,0.5]));
                controller._locatorMarkerGraphicsLayer = new esri.layers.GraphicsLayer();

                // Create the marker symbol
                controller.markerSymbol = new PictureMarkerSymbol({
                    "angle":0,
                    "xoffset":0,
                    "yoffset":13,
                    "type":"esriPMS",
                    "url":"images/green-pin.png",
                    "width":35,
                    "height":35
                })
                
                esriConfig.defaults.io.corsEnabledServers.push("servicesbeta.esri.com");
                esriConfig.defaults.io.corsEnabledServers.push("s3.amazonaws.com");
                
                
                 // GeoJSON layer
                controller.featureLayer = [];
                var espacesVertsContour = new SimpleLineSymbol("solid", new Color(controller.geoJSONDataset["parcespacevert"].color), 4);
                var attraitsTouristiqueContour = new SimpleLineSymbol("solid", new Color(controller.geoJSONDataset["edificesmunicipaux"].color), 4);
                    
                controller.geoJSONDataset["parcespacevert"].symbol = new SimpleFillSymbol("solid", espacesVertsContour, new Color(controller.geoJSONDataset["parcespacevert"].color));
                controller.geoJSONDataset["pistecyclable"].symbol = new SimpleLineSymbol("solid", new Color(controller.geoJSONDataset["pistecyclable"].color), 4);
                controller.geoJSONDataset["InfraPieton"].symbol = new SimpleLineSymbol("solid", new Color(controller.geoJSONDataset["InfraPieton"].color), 2);
                controller.geoJSONDataset["edificesmunicipaux"].symbol = new SimpleFillSymbol("solid", attraitsTouristiqueContour, new Color(controller.geoJSONDataset["edificesmunicipaux"].color));
                
                controller.init();  
            }
    );
