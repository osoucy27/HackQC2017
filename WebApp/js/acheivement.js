/*
 * acheivement.js. Manage success. Receive input from player.
 */

var acheivement = acheivement || {};

acheivement.badges = [
    {img_node_id: 'success1', image_success: 'images/succes_couleur.png', min_pts : 10, received: false},
    {img_node_id: 'success2', image_success: 'images/succes_couleur.png', min_pts : 100, received: false},
    {img_node_id: 'success3', image_success: 'images/succes_couleur.png', min_pts : 1000, received: false}
]

var promos = [
    {img_node_id: 'velo50', min_pts : 1, received: false},
    {img_node_id: 'chaussure1000', min_pts : 10, received: false},
    {img_node_id: 'brulerie1000', min_pts : 1000, received: false}
]

acheivement.updateBadgesStatus = function (new_pts_count)
{
    acheivement.badges.forEach(function (badge) {
        if (!badge.received && new_pts_count >= badge.min_pts)
        {
            badge.received = true;
            acheivement.updateImg(badge);
            acheivement.notifyNewBadgeReceived(badge);
        }
    });
}

promos.updateBadgesStatus = function (new_pts_count)
{
    promos.forEach(function (promo) {
        if (!promo.received && new_pts_count >= promo.min_pts)
        {
            promos.updateImg(promo);
        }
    });
}

promos.updateImg = function (promo)
{
    $("#" + promo.img_node_id)[0].attributes["class"].value = "ui-btn ui-btn-up-c ui-btn-icon-right ui-li-has-arrow ui-li ui-last-child";
}

acheivement.updateImg = function (badge)
{
    $("#" + badge.img_node_id)[0].attributes["src"].value = badge.image_success;
}

acheivement.notifyNewBadgeReceived = function (badge)
{
    $.mobile.changePage("#dialogBadge", 'pop');
}
