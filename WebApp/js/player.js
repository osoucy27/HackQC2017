/*
 * Player.js implements the accumulation of points.
 */

function deg2rad(deg)
{
    return deg * (Math.PI/180);
}

function getDistanceFromLatLonInMeters(lat1,lon1,lat2,lon2) 
{
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d * 1000;
}

var player = player || {};

player.points = 0;
player.last_position = null;
//in timestamp
player.last_position_time = 0;
player.last_rest_time = 0;

/*
 * Take in parameter the new_position of the player. {lat: <lat>, lon: <lon>}
 * Take also the time the new_position is calculated.
 * Accumulate points if there is a last_position and
 * the player move a certain distance.
 */
player.accumulateWithNewPos = function(new_position, time)
{
    if (time <= player.last_position_time)
    {
        console.log("time should be greater than player.last_position_time");
    }
    player.callWBSZone(new_position, player.updateLocation);
    if (player.last_position != null && 
            player.isMoving(
                player.last_position,
                player.last_position_time, 
                new_position,
                time))
    {
        if(player.hasAccumulatePoint(time))
        {
            player.points += 1;
            player.callWBSZone(new_position, player.calculateBonus);
            acheivement.updateBadgesStatus(player.points);
			promos.updateBadgesStatus(player.points);
            player.last_rest_time = time;
        }
    }
    else
    {
        player.last_rest_time = time;
    }

    player.last_position = new_position;
    player.last_position_time = time;
    

    $("#draw-score").html(player.points);
    $("#word-point").html(player.points > 1 ? " points" : " point");

}

player.isMoving = function(last_position, last_position_time, new_position, time)
{
    //in meters.
    travelled_distance = getDistanceFromLatLonInMeters(
            last_position.lat,
            last_position.lon,
            new_position.lat,
            new_position.lon);

    //in seconds.
    passed_time = time - last_position_time;
    //in meter / seconds
    travel_speed = travelled_distance / passed_time;

    //A phone that move at a speed of 0.5 m/s is moving.
    return travel_speed >= 0.5;
}

player.hasAccumulatePoint = function(time)
{
    //A person that has moved for the last 5 seconds earns points.
    return time - player.last_rest_time >= 5;
}


var location_types = [
    {img_node_id: 'cycle', image_success: 'images/cycliste_lumineux.png', image_off: 'images/cycliste.png', code:1, received: false},
	{img_node_id: 'parc', image_success: 'images/arbre_lumineux.png', image_off: 'images/arbre.png', code:2, received: false},
    {img_node_id: 'pieton', image_success: 'images/pieton_lumineux.png', image_off: 'images/pieton.png', code:3, received: false}
]

player.updateLocation = function(data, code_status, xhr)
{
    position_code = data.points;

    for(var i = 0; i<location_types.length; ++i){
        if(position_code.indexOf(location_types[i].code) != -1){
            $("#" + location_types[i].img_node_id)[0].attributes["src"].value = location_types[i].image_success;
        } else {
            $("#" + location_types[i].img_node_id)[0].attributes["src"].value = location_types[i].image_off;
        }
    }
}

player.calculateBonus = function(data, code_status, xhr)
{
    console.log(code_status);
    if (data.points.length > 0)
    {
        console.log("Player earn a bonus :).");
        player.points += 1;
        //Asynchronous. So I have to update acheivement there also.
        acheivement.updateBadgesStatus(player.points);
    }
}

player.callWBSZone = function(new_position, success_function)
{
    url_webservice = "https://sample-env-1.ikjqpbsrt2.us-east-1.elasticbeanstalk.com/position";
    $.ajax({
            url: url_webservice,
            data: {lat: new_position.lat, lng: new_position.lon},
            method: 'GET',
            success: success_function
    });
}
