//Test getDistanceFromLatLonInMeters
{
    distance = getDistanceFromLatLonInMeters(45.420763, -71.9664, 45.419881, -71.965950);
    if (distance >= 150 && distance <= 50)
    {
        console.log("getDistanceFromLatLonInMeters test failed\n");
        console.log(distance);
    }
}

//Test Player Init
{
    if (player.points != 0)
    {
        console.log("Player points is not initialized");
    }
    if (player.last_position != null)
    {
        console.log("Last position is not initialized");
    }
    if (player.last_position_time != 0)
    {
        console.log("Last position time is not initialized.")
    }
    if (player.last_rest_time != 0)
    {
        console.log("Last rest time is not initialized.")
    }
}

//Test Player isMoving return false.
{
    if (player.isMoving(
                {lat: 45.420763, lon: -71.9664}, 0,
                {lat: 45.420770, lon: -71.9665}, 20))
    {
        console.log("Player shouldn't move here.");
    }
}

//Test Player isMoving return true.
{
    if (!player.isMoving(
                {lat: 45.420763, lon: -71.9664}, 0,
                {lat: 45.419881, lon: -71.965950}, 20))
    {
        console.log("Player should move here.");
    }
}

/*
 * The next tests are not unit tests. They will change the state
 * of player and they must be executed in order to pass.
 */
//Test First call accumulateWithNewPos
{
    position = {lat: 45.420763, lon: -71.9664};
    time = 30;
    player.accumulateWithNewPos(position, time);
    if (player.last_position !== position)
    {
        console.log("Last position should be position.");
    }
    if (player.last_position_time != player.last_rest_time && 
            player.last_rest_time != time)
    {
        console.log("Time should be time.");
    }
}

//Test call accumulateWithNewPos not moving.
{
    position = {lat: 45.420760, lon: -71.9663};
    time = 50;
    player.accumulateWithNewPos(position, time);
    if (player.last_position !== position)
    {
        console.log("Last position should be position.");
    }
    if (player.last_position_time != player.last_rest_time &&
            player.last_rest_time != time)
    {
        console.log("Time should be time.");
    }
}

//Test call accumulateWithNewPos moving 1.
{
    old_last_rest_time = player.last_rest_time;
    position = {lat: 45.419881, lon: -71.965950};
    time = 100;
    player.accumulateWithNewPos(position, time);
    if (player.last_position !== position)
    {
        console.log("Last position should be position.");
    }
    if (player.last_position_time != time)
    {
        console.log("Time should be time.");
    }
    if (old_last_rest_time != player.last_rest_time)
    {
        console.log("Player.last_rest_time should not change.");
    }
}

//Test call accumulateWithNewPos moving 2 and add one point
{
    position = {lat: 45.420760, lon: -71.9663};
    time = 130;
    player.accumulateWithNewPos(position, time);
    if (player.last_position !== position)
    {
        console.log("Last position should be position.");
    }
    if (player.last_position_time != time)
    {
        console.log("Time should be time.");
    }
    if (player.points != 1)
    {
        console.log("player should have his first point.");
    }
}
