package com.hackqc.ventnord.postgre.dataload.pisteCyclable;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.Geometry;
import org.geojson.LineString;
import org.geojson.LngLatAlt;
import org.geojson.MultiLineString;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RimouskiTrottoir {

private static Connection connection=null;

	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, NoSuchAuthorityCodeException, FactoryException, ClassNotFoundException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		
		FeatureCollection o = mapper.readValue(new File(Rimouski.class.getClassLoader().getResource("infrapieton.json").getFile()), FeatureCollection.class);
		
		Feature original1 = o.getFeatures().get(0);
	
		adjust(o);
		
	}
	
	private static void adjust(FeatureCollection o) throws NoSuchAuthorityCodeException, FactoryException, ClassNotFoundException, SQLException{
		int initialCount = 0;
		int addedCount = 0;
		
		for(Feature f : o.getFeatures()){
			Geometry<LngLatAlt> g = (Geometry<LngLatAlt>) f.getGeometry();
			System.out.println(f.getProperties().get("OBJECTID"));
			if(f.getGeometry() instanceof LineString){
				Pieton p = new Pieton(
					(Integer)f.getProperties().get("OBJECTID"),
				    (Double)f.getProperties().get("SHAPE.STLength()"),
				    (String)f.getProperties().get("Type"),
				    ((LineString)f.getGeometry()).getCoordinates(),
				    "LINESTRING"
			    );
				
				insert(p);
			} else if(f.getGeometry() instanceof MultiLineString){
				
				MultiLineString m = (MultiLineString) f.getGeometry();
				for(List<LngLatAlt> line : m.getCoordinates()){
					Pieton p = new Pieton(
							(Integer)f.getProperties().get("OBJECTID"),
						    (Double)f.getProperties().get("SHAPE.STLength()"),
						    (String)f.getProperties().get("Type"),
						    line,
						    "MULTILINESTRING"
					);
					insert(p);
				}
				
			} else {
				throw new IllegalStateException();
			}

		}
		System.out.println("Initial count : " + initialCount);
		System.out.println("After added points : " + addedCount);
	}
	
	private static void insert(Pieton p) throws ClassNotFoundException, SQLException{
		Class.forName("org.postgresql.Driver");
		
		Connection c = getConnection();
		String sql = "INSERT INTO public.linestring( "
				     + "object_id, length, pieton_type, line, type, file) "
				     + "VALUES (?, ?, ?, ST_GeomFromText(?,4326),?,?);";
		
		PreparedStatement st = c.prepareStatement(sql);
		st.setInt(1, p.getObjectId());
		st.setDouble(2, p.getLength());
		st.setString(3, p.getPietonType());
		
		String line = "LINESTRING(";
		for(LngLatAlt l : p.getLngLat()){
			if(!line.equals("LINESTRING(")){
				line = line + ",";
			}
			line = line + l.getLongitude() + " " + l.getLatitude();
		}
		line = line + ")";
		
		st.setString(4, line);
		st.setString(5, p.getType());
		st.setString(6, "infrapieton.json");
		
		int i = st.executeUpdate();
		
	}
	
	private static Connection getConnection() throws SQLException{
		if(connection == null){
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
		}
		
		return connection;
	}

}
