package com.hackqc.ventnord.postgre.dataload.pisteCyclable;

import java.util.List;

import org.geojson.LngLatAlt;

public class PisteCyclable {
	public String getType() {
		return type;
	}

	final int objectId;
	final double length;
	final String typeRevtCyc;
	final String typeVoieCyc;
	final List<LngLatAlt> lngLat;
	final String type;
	
	public PisteCyclable(int objectId, double length, String typeRevtCyc, String typeVoieCyc, List<LngLatAlt> lngLat, String type) {
		super();
		this.objectId = objectId;
		this.length = length;
		this.typeRevtCyc = typeRevtCyc;
		this.typeVoieCyc = typeVoieCyc;
		this.lngLat = lngLat;
		this.type = type;
	}

	public int getObjectId() {
		return objectId;
	}

	public double getLength() {
		return length;
	}

	public String getTypeRevtCyc() {
		return typeRevtCyc;
	}

	public String getTypeVoieCyc() {
		return typeVoieCyc;
	}

	public List<LngLatAlt> getLngLat() {
		return lngLat;
	}
}
