package com.hackqc.ventnord.postgre.dataload.parc;

import java.util.List;

import org.geojson.LngLatAlt;

public class Parc {
	
	final int objectId;
	final String nom;
	final List<LngLatAlt> lngLat;
	final String type;
	
	public int getObjectId() {
		return objectId;
	}

	public String getNom() {
		return nom;
	}

	public List<LngLatAlt> getLngLat() {
		return lngLat;
	}

	public Parc(int objectId, String nom, List<LngLatAlt> lngLat, String type) {
		super();
		this.objectId = objectId;
		this.nom = nom;
		this.lngLat = lngLat;
		this.type = type;
	}

	public String getType() {
		return type;
	}
	

}
