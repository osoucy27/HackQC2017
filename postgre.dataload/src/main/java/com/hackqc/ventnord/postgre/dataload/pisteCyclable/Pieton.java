package com.hackqc.ventnord.postgre.dataload.pisteCyclable;

import java.util.List;

import org.geojson.LngLatAlt;

public class Pieton {
	
	final int objectId;
	final double length;
	final String pietonType;
	final List<LngLatAlt> lngLat;
	final String type;
	
	public Pieton(int objectId, double length, String pietonType, List<LngLatAlt> lngLat, String type) {
		this.objectId = objectId;
		this.length = length;
		this.pietonType = pietonType;
		this.lngLat = lngLat;
		this.type = type;
	}

	public int getObjectId() {
		return objectId;
	}

	public double getLength() {
		return length;
	}

	public String getPietonType() {
		return pietonType;
	}

	public List<LngLatAlt> getLngLat() {
		return lngLat;
	}

	public String getType() {
		return type;
	}
	
	

}
