package com.hackqc.ventnord.postgre.dataload.parc;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.Geometry;
import org.geojson.LineString;
import org.geojson.LngLatAlt;
import org.geojson.MultiLineString;
import org.geojson.MultiPolygon;
import org.geojson.Polygon;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackqc.ventnord.postgre.dataload.pisteCyclable.PisteCyclable;

public class Rimouski {

	private static Connection connection=null;

	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, NoSuchAuthorityCodeException, FactoryException, ClassNotFoundException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		
		FeatureCollection o = mapper.readValue(new File(Rimouski.class.getClassLoader().getResource("parcespacevert.json").getFile()), FeatureCollection.class);
	
		adjust(o);
		
	}
	
	private static void adjust(FeatureCollection o) throws NoSuchAuthorityCodeException, FactoryException, ClassNotFoundException, SQLException{
		int initialCount = 0;
		int addedCount = 0;
		
		int objectId = 1;
		
		for(Feature f : o.getFeatures()){
			Geometry<LngLatAlt> g = (Geometry<LngLatAlt>) f.getGeometry();
			System.out.println(objectId);
			if(f.getGeometry() instanceof Polygon){
				Polygon m = (Polygon) f.getGeometry();
				for(List<LngLatAlt> line : m.getCoordinates()){
					Parc p = new Parc(
							objectId,
							(String)f.getProperties().get("Nom"),
							line,
							"POLYGON"
					);
					insert(p);
					objectId++;
				}
				
				
			} else if(f.getGeometry() instanceof MultiPolygon){
				
				MultiPolygon m = (MultiPolygon) f.getGeometry();
				for(List<List<LngLatAlt>> line : m.getCoordinates()){
					for(List<LngLatAlt> l1 : line){
						Parc p = new Parc(
								objectId,
								(String)f.getProperties().get("Nom"),
								l1,
								"MULTIPOLYGON"
						);
						insert(p);
					}
					objectId++;
				}
				
			} else {
				throw new IllegalStateException();
			}

		}
		System.out.println("Initial count : " + initialCount);
		System.out.println("After added points : " + addedCount);
	}
	
	private static void insert(Parc p) throws ClassNotFoundException, SQLException{
		Class.forName("org.postgresql.Driver");
		
		Connection c = getConnection();
		String sql = "INSERT INTO public.polygon( "
				     + "object_id, nom, polygon, type, file) "
				     + "VALUES (?, ?, ST_GeomFromText(?,4326), ?, ?);";
		
		PreparedStatement st = c.prepareStatement(sql);
		st.setInt(1, p.getObjectId());
		st.setString(2, p.getNom());
		
		String line = "POLYGON((";
		for(LngLatAlt l : p.getLngLat()){
			if(!line.equals("POLYGON((")){
				line = line + ",";
			}
			line = line + l.getLongitude() + " " + l.getLatitude();
		}
		line = line + "))";
		
		st.setString(3, line);
		st.setString(4, p.getType());
		st.setString(5, "parcespacevert.json");
		
		int i = st.executeUpdate();
		
	}
	
	private static Connection getConnection() throws SQLException{
		if(connection == null){
			connection = DriverManager.getConnection(
					"jdbc:postgresql://mapposition.c3glvd4sffwj.us-east-1.rds.amazonaws.com:5432/mapposition", "root",
					"hackqc2017");
		}
		
		return connection;
	}

}
